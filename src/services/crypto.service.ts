import CryptoAES from "crypto-js/aes";
import CryptoENC from "crypto-js/enc-utf8";

class CryptoService {
  /**
   * Decrypt a cipher string
   *
   * @param   cipherText - Ciphered text
   * @param   secret     - Secret decryption key
   * @returns Decrypted cipher text
   */
  decrypt(cipherText: string, secret: string): string {
    const bytes = CryptoAES.decrypt(cipherText, secret);
    return CryptoENC.stringify(bytes);
  }

  /**
   * Encrypt a string using a secret key
   *
   * @param   plainText - Plain text
   * @param   secret    - Secret encryption key
   * @returns Encrypted string
   */
  encrypt(plainText: string, secret: string): string {
    return CryptoAES.encrypt(plainText, secret).toString();
  }

  /**
   * Generate a psuedo-random alpha-numeric string
   *
   * Source: https://stackoverflow.com/a/10727155/4206438
   *
   * @param   length - Desired string length
   * @returns Psuedo-random alpha-numeric string
   */
  randomString(length = 16): string {
    const chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"; // prettier-ignore
    let result = "";
    for (let i = length; i > 0; --i) {
      result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
  }
}

const singleton = new CryptoService();
export default singleton;
