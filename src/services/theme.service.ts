// Types
import { Theme, themeStrings } from "@typings/theme.types";

const THEME_KEY = "user-theme";

class ThemeService {
  save(theme: Theme): void {
    localStorage.setItem(THEME_KEY, theme);
  }

  load(): Theme {
    const theme = localStorage.getItem(THEME_KEY) as Theme;
    if (!theme) return "light-theme";

    if (!themeStrings.includes(theme)) return "light-theme";
    return theme;
  }
}

const singleton = new ThemeService();
export default singleton;
