// Types
import { INote, INoteBase } from "@typings/note.types";

class NoteService {
  create(note: INote): void {
    localStorage.setItem(note.id, note.data);
  }

  fetch(id: string): INoteBase | null {
    const note = localStorage.getItem(id);
    if (!note) return null;

    return {
      id,
      destroyedAt: null,
    };
  }

  read(id: string): INote | null {
    const note = localStorage.getItem(id);
    if (!note) return null;

    return {
      data: note,
      destroyedAt: null,
      id,
    };
  }
}

const singleton = new NoteService();
export default singleton;
