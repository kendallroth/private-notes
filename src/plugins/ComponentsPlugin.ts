import { App } from "vue";

// Components
import { ActionBar } from "@components/layout";
import { PageTitle } from "@components/typography";
import SvgIcon from "@components/SvgIcon.vue";

export default {
  install: (app: App): void => {
    app.component("action-bar", ActionBar);
    app.component("page-title", PageTitle);
    app.component("svg-icon", SvgIcon);
  },
};
