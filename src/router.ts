import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

// Components
import {
  NoteCreated,
  NoteDecrypt,
  NoteDisplay,
  NoteEncrypt,
} from "@views/Note";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    redirect: {
      name: "NoteEncrypt",
    },
  },
  {
    path: "/note/new",
    name: "NoteEncrypt",
    component: NoteEncrypt,
  },
  {
    // NOTE: Requires 'link' prop from 'NoteEncrypt' route
    path: "/note/created",
    name: "NoteCreated",
    component: NoteCreated,
    props: true,
  },
  {
    path: "/note/:id",
    name: "NoteDecrypt",
    component: NoteDecrypt,
  },
  {
    // NOTE: Requires 'note' prop from 'NoteDecrypt' route
    path: "/note/read",
    name: "NoteDisplay",
    component: NoteDisplay,
    props: true,
  },
  {
    path: "/about",
    name: "About",
    component: () =>
      import(/* webpackChunkName: "about" */ "@views/About/About.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
