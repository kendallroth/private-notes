import "@fontsource/mulish/variable.css";

import {
  create as createNaive,
  NAlert,
  NButton,
  NCard,
  NIcon,
  NInput,
  NMessageProvider,
  NModal,
  NSpace,
  NSpin,
} from "naive-ui";
import { createApp } from "vue";

// Components
import App from "./App.vue";

// Utilities
import ComponentsPlugin from "@plugins/ComponentsPlugin";
import "./registerServiceWorker";
import router from "./router";

// Styles
import "@styles/app.scss";

const app = createApp(App);

const naive = createNaive({
  components: [
    NAlert,
    NButton,
    NCard,
    NIcon,
    NInput,
    NMessageProvider,
    NModal,
    NSpace,
    NSpin,
  ],
});

// Apply plugins
app.use(router);
app.use(naive);
app.use(ComponentsPlugin);

// Mount application
app.mount("#app");
