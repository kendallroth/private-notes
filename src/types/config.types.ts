/** Web application configuration */
export interface IWebConfig {
  /** App configuration */
  app: IWebAppConfig;
  /** API configuration */
  api: IWebApiConfig;
}

/** App-specific configuration */
export interface IWebAppConfig {
  /** Deployed app url */
  appUrl: string;
  /** App environment name */
  envName: string;
  /** Whether app is deployed in production environment */
  production: boolean;
  /** Web app version */
  version: string;
}

/** API configuration */
export interface IWebApiConfig {
  /** API url */
  apiUrl: string;
}
