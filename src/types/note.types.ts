/** Basic note data */
export interface INoteBase {
  /** Note ID */
  id: string;
  /** When the note was destroyed */
  destroyedAt: Date | null;
}

/** Extended note data */
export interface INote extends INoteBase {
  /** Note data (encrypted/decrypted data) */
  data: string;
}
