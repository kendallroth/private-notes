export const themeStrings = ["light-theme", "dark-theme"] as const;
export type Theme = typeof themeStrings[number];
