// Utilities
import { version } from "../package.json";

// Types
import { IWebConfig } from "@typings/config.types";

const { VUE_APP_API_URL: apiUrl, VUE_APP_APP_URL: appUrl } = process.env;

if (!apiUrl || !appUrl) {
  throw new Error("Missing environment variables");
}

const webConfig: IWebConfig = {
  app: {
    appUrl: process.env.VUE_APP_APP_URL,
    envName: process.env.VUE_APP_ENV_NAME ?? "local",
    production: process.env.NODE_ENV === "production",
    version,
  },
  api: {
    apiUrl: process.env.VUE_APP_API_URL,
  },
};

export default webConfig;
