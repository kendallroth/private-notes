// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require("path");

const src = path.resolve(__dirname, "src");

module.exports = {
  lintOnSave: false,

  css: {
    loaderOptions: {
      scss: {
        prependData: `
          @import "~@styles/_vars.scss";
          @import "~@styles/_breakpoints.scss";
        `,
      },
    },
  },

  configureWebpack: {
    resolve: {
      alias: {
        "@": src,
        "@assets": path.join(src, "assets"),
        "@components": path.join(src, "components"),
        "@config": path.join(src, "config"),
        "@plugins": path.join(src, "plugins"),
        "@router": path.join(src, "router"),
        "@services": path.join(src, "services"),
        "@store": path.join(src, "store"),
        "@styles": path.join(src, "styles"),
        "@typings": path.join(src, "types"),
        "@utilities": path.join(src, "utilities"),
        "@views": path.join(src, "views"),
      },
    },
  },

  pwa: {
    name: "Private Notes",
  },
};
